package com.androidserver.server.controller;

import com.androidserver.server.entiy.score;
import com.androidserver.server.service.scoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.springboot.common.Result;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/score")
public class scoreController {
    @Autowired
    scoreService scoreservice;
    @GetMapping("/list/{userid}")//测试通过
    public Result list(@PathVariable Integer userid){
        List<score> scores=scoreservice.list(userid);
        return Result.success(scores);
    }
    @GetMapping("/listscores")//测试通过
    public Result listscores() {
        List<score> scores=scoreservice.listscores();
        return Result.success(scores);
    }

    @PostMapping("/save")//测试通过
    public Result save(@RequestBody score score) {
        try {
            scoreservice.save(score);
            return Result.success();
        } catch (Exception e) {
            return  Result.error(e.toString());
        }
    }

    @PutMapping("/update")//测试成功
    public Result update(@RequestBody score score) {
        try {
            scoreservice.update(score);
            return Result.success();
        } catch (Exception e) {
            return  Result.error(e.toString());
        }
    }

   @DeleteMapping("/delete")//测试成功
    public Result delete(@RequestBody score score) {
       try {
           scoreservice.delete(score);
           return Result.success();
       } catch (Exception e) {
           return  Result.error(e.toString());
       }
   }
}
