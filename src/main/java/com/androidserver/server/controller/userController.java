package com.androidserver.server.controller;

import com.androidserver.server.entiy.user;
import com.androidserver.server.service.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.springboot.common.Result;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class userController {
    @Autowired(required = false)
    userService userservice;
    @GetMapping("/list")//测试通过
    public Result list(){
        List<user> users=userservice.list();
        return Result.success(users);
    }
    @GetMapping("/{id}")//测试通过
    public Result getById(@PathVariable Integer id){
        user user=userservice.getById(id);
        return Result.success(user);
    }
    @PostMapping("/login")//测试通过
    public Result login(@RequestBody user request){
        user user=userservice.getByNameandPwd(request);
        if(user==null) return Result.error("用户名或密码错误");
        return Result.success(user);
    }
    @PostMapping("/save")//测试通过
    public Result save(@RequestBody user request){
        try {
            userservice.save(request);
            return Result.success();
        } catch (Exception e) {
            return  Result.error(e.toString());
        }
    }

    @DeleteMapping("/delete/{id}")//测试通过
    public Result delete(@PathVariable Integer id){
        try {
            userservice.delete(id);
            return Result.success();
        } catch (Exception e) {
            return  Result.error(e.toString());
        }
    }
    @PutMapping("/update")//测试通过
    public Result update(@RequestBody user request){
        try {
            userservice.update(request);
            return Result.success();
        } catch (Exception e) {
            return  Result.error(e.toString());
        }
    }

}
