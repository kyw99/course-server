package com.androidserver.server.controller;

import com.androidserver.server.entiy.course;
import com.androidserver.server.service.courseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.springboot.common.Result;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/course")
public class courseController {
    @Autowired(required = false)
    courseService courseservice;

    @GetMapping("/list")//测试通过
    public Result list(){
        List<course> courses = courseservice.list();
        return Result.success(courses);
    }
    @PostMapping("/save")//测试通过
    public Result save(@RequestBody course course){
        try {
            courseservice.save(course);
            return Result.success();
        } catch (Exception e) {
            return  Result.error(e.toString());
        }
    }
    @DeleteMapping("/delete/{id}")//测试通过
    public Result delete(@PathVariable Integer id){
        try {
            courseservice.delete(id);
            return Result.success();
        } catch (Exception e) {
            return  Result.error(e.toString());
        }
    }
    @PutMapping("/update")//
    public Result update(@RequestBody course course){
        try {
            courseservice.update(course);
            return Result.success();
        } catch (Exception e) {
            return  Result.error(e.toString());
        }
    }
}
