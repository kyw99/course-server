package com.androidserver.server.request;

import lombok.Data;

@Data
public class loginRequest {
    String name;
    String password;
    Integer type;
}
