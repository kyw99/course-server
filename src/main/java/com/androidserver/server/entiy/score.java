package com.androidserver.server.entiy;

import lombok.Data;

@Data
public class score {
    Integer userid;
    Integer courseid;
    String name;
    String coursename;
    String score;
}
