package com.androidserver.server.mapper;

import com.androidserver.server.entiy.score;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;

import java.util.List;

@Mapper
public interface scoreMapper {
    @Select("CALL proc_name(#{userid, mode=IN, jdbcType=INTEGER})")
    @Options(statementType = StatementType.CALLABLE)
    List<score> list(Integer userid);

    @Select("CALL proc_name2()")
    @Options(statementType = StatementType.CALLABLE)
    List<score> listscores();

    @Insert("insert into score values(#{userid},#{courseid},#{score})")
    void save(score score);

    @Update("update score set score=#{score} where userid=#{userid} and courseid=#{courseid}")
    void update(score score);

    @Delete("delete from score where userid=#{userid} and courseid=#{courseid}")
    void delete(score score);


}
