package com.androidserver.server.mapper;

import com.androidserver.server.entiy.course;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface courseMapper {
    @Select("Select * from course")
    List<course> list();
    @Insert("insert into course(coursename,credit) values(#{coursename},#{credit})")
    void save(course course);
    @Delete("delete from course where id=#{id}")
    void delete(Integer id);
    @Update("update course set coursename=#{coursename},credit=#{credit} where id=#{id}")
    void update(course course);
}
