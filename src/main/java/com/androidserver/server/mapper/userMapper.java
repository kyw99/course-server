package com.androidserver.server.mapper;

import com.androidserver.server.entiy.user;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface userMapper {
    @Select("select * from user")
    List<user> list();//查所有
    @Select("select * from user where id=#{id}")
    user getById(Integer id);//查特定
    @Select("select * from user where name=#{name} and password=#{password}")
    user getByNameandPwd(user user);//用于登录和查重
    @Insert("insert into user(name,password,type) values(#{name},#{password},#{type})")
    void save(user user);//增
    @Delete("delete from user where id=#{id}")
    void delete(Integer id);//删
    @Update("update user set name=#{name},password=#{password},type=#{type} where id=#{id}")
    void update(user user);//改
}
