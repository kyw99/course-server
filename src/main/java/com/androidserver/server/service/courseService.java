package com.androidserver.server.service;

import com.androidserver.server.entiy.course;
import com.androidserver.server.mapper.courseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface courseService {
    List<course> list();

    void save(course course);

    void delete(Integer id);

    void update(course course);

}
