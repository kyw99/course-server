package com.androidserver.server.service;

import com.androidserver.server.entiy.score;


import java.util.List;

public interface scoreService {
    List<score> list(Integer userid);//查用户的所有成绩

    List<score> listscores();

    void save(score score);

    void update(score score);

    void delete(score score);
}
