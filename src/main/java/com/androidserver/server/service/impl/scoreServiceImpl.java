package com.androidserver.server.service.impl;

import com.androidserver.server.entiy.score;
import com.androidserver.server.mapper.scoreMapper;
import com.androidserver.server.service.scoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class scoreServiceImpl implements scoreService {
    @Autowired(required = false)
    scoreMapper scoremapper;

    @Override
    public List<score> list(Integer userid) { return scoremapper.list(userid); }

    @Override
    public List<score> listscores() { return scoremapper.listscores(); }

    @Override
    public void save(score score) { scoremapper.save(score); }

    @Override
    public void update(score score) { scoremapper.update(score); }

    @Override
    public void delete(score score) { scoremapper.delete(score); }
}
