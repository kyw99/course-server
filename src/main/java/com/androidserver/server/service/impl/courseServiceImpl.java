package com.androidserver.server.service.impl;

import com.androidserver.server.entiy.course;
import com.androidserver.server.mapper.courseMapper;
import com.androidserver.server.service.courseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class courseServiceImpl implements courseService {
    @Autowired(required = false)
    courseMapper coursemapper;
    @Override
    public List<course> list() { return coursemapper.list(); }

    @Override
    public void save(course course) { coursemapper.save(course); }

    @Override
    public void delete(Integer id) { coursemapper.delete(id); }

    @Override
    public void update(course course) { coursemapper.update(course); }
}
