package com.androidserver.server.service.impl;

import com.androidserver.server.entiy.user;
import com.androidserver.server.mapper.userMapper;
import com.androidserver.server.service.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.hutool.crypto.SecureUtil;

import java.util.List;

@Service
public class userServiceImpl implements userService {
    @Autowired(required = false)
    userMapper usermapper;

    @Override
    public List<user> list() { return usermapper.list(); }

    @Override
    public user getById(Integer id) { return usermapper.getById(id); }

    @Override
    public user getByNameandPwd(user user) {
        user.setPassword(securePass(user.getPassword()));
        return usermapper.getByNameandPwd(user);
    }

    @Override
    public void save(user user) { 
        user.setPassword(securePass(user.getPassword()));
        usermapper.save(user); 
    }

    @Override
    public void delete(Integer id) { usermapper.delete(id); }

    @Override
    public void update(user user) {
        user.setPassword(securePass(user.getPassword()));
        usermapper.update(user);
    }

    private String securePass(String password){
        return SecureUtil.md5(password);
    }
}
