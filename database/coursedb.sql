/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : localhost:3306
 Source Schema         : coursedb

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 05/06/2023 15:43:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `coursename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `credit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, '编译原理', '2.5');
INSERT INTO `course` VALUES (2, '移动应用基础教程', '3');
INSERT INTO `course` VALUES (3, '算法设计与分析', '3');
INSERT INTO `course` VALUES (4, 'Linux', '3');

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `userid` int NOT NULL,
  `courseid` int NOT NULL,
  `score` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  INDEX `userId`(`userid` ASC) USING BTREE,
  INDEX `courseId`(`courseid` ASC) USING BTREE,
  CONSTRAINT `score_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `score_ibfk_2` FOREIGN KEY (`courseid`) REFERENCES `course` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES (1, 1, '100');
INSERT INTO `score` VALUES (1, 2, '99');
INSERT INTO `score` VALUES (1, 3, '95');
INSERT INTO `score` VALUES (1, 4, NULL);


-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` int NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '孔耀威', 1, '202cb962ac59075b964b07152d234b70');
INSERT INTO `user` VALUES (2, 'test', 0, '202cb962ac59075b964b07152d234b70');


-- ----------------------------
-- Procedure structure for proc_name
-- ----------------------------
DROP PROCEDURE IF EXISTS `proc_name`;
delimiter ;;
CREATE PROCEDURE `proc_name`(IN userid INT)
BEGIN
    SELECT s.*, u.name AS name, c.coursename AS coursename 
    FROM score s 
    JOIN user u ON s.userid = u.id 
    JOIN course c ON s.courseid = c.id 
    WHERE s.userid = userid;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for proc_name2
-- ----------------------------
DROP PROCEDURE IF EXISTS `proc_name2`;
delimiter ;;
CREATE PROCEDURE `proc_name2`()
BEGIN
    SELECT s.*, u.name AS name, c.coursename AS coursename 
    FROM score s 
    JOIN user u ON s.userid = u.id 
    JOIN course c ON s.courseid = c.id ;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table course
-- ----------------------------
DROP TRIGGER IF EXISTS `duplication`;
delimiter ;;
CREATE TRIGGER `duplication` BEFORE INSERT ON `course` FOR EACH ROW BEGIN
    IF EXISTS(SELECT * FROM course WHERE coursename = NEW.coursename) THEN
        SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'Course already exists';
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table score
-- ----------------------------
DROP TRIGGER IF EXISTS `duplication2`;
delimiter ;;
CREATE TRIGGER `duplication2` BEFORE INSERT ON `score` FOR EACH ROW IF EXISTS(SELECT * FROM score WHERE courseid = NEW.courseid and userid=NEW.userid ) THEN
        SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'score already exists';
    END IF
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table user
-- ----------------------------
DROP TRIGGER IF EXISTS `duplication3`;
delimiter ;;
CREATE TRIGGER `duplication3` BEFORE INSERT ON `user` FOR EACH ROW BEGIN
    IF EXISTS(SELECT * FROM  `user` WHERE name = NEW.name AND type = NEW.type) THEN
        SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'USER already exists';
    END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table user
-- ----------------------------
DROP TRIGGER IF EXISTS `deleterelate`;
delimiter ;;
CREATE TRIGGER `deleterelate` BEFORE DELETE ON `user` FOR EACH ROW BEGIN
    delete from score where userid=OLD.id;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
